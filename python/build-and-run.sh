#!/bin/bash

echo Checking for pyenv
brew list pyenv > /dev/null || brew install pyenv

echo Checking for pipenv
if ! pip list | grep -F pipenv; then
	pip install pipenv
fi

echo Installing packages
pipenv install --deploy

echo Ready to go!
pipenv run python simple.py 
