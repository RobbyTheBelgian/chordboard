import scamp
import tkinter as tk
import config
from PySide2.QtCore import Qt, Slot
from PySide2.QtWidgets import (
    QApplication,
    QPushButton,
    QWidget,
    QGridLayout,
    QSizePolicy,
)
import sys


class Player:
    pass


class GUI:
    pass


WIDTH = 2000

COLORS = ["#DDDDDD"] * 12
COLORS[0] = "#FF5555"
COLORS[4] = "#55FF55"
COLORS[8] = "#5555FF"

COLOR_ON_C = True

conf = config.config


SESSION = scamp.Session()
INSTRUMENT = SESSION.new_part(conf.INSTRUMENT)

ROW_INSTRUMENTS = []
for flavor in conf.CHORD_FLAVORS:
    if flavor.instrument is None:
        ROW_INSTRUMENTS.append(INSTRUMENT)
    else:
        ROW_INSTRUMENTS.append(SESSION.new_part(flavor.instrument))


def shift(note, offset):
    x = note + offset + conf.NOTE_BASIS
    while x > conf.HIGHEST:
        x -= 12
    while x < conf.LOWEST:
        x += 12
    return x


def play_chord(notes, offset, instrument, arpeg=config.Arpegiation.NONE):
    instrument.play_chord([shift(x, offset) for x in notes], 1, conf.MAX_NOTE_LENGTH)


def start_chord(notes, offset, instrument, arpeg=config.Arpegiation.NONE):
    instrument.end_all_notes()
    instrument.start_chord([shift(x, offset) for x in notes], 1)


def end_chord(instrument):
    instrument.end_all_notes()


def simple():
    window = tk.Tk()
    window.geometry(f"{WIDTH}x{WIDTH//25*len(conf.CHORD_FLAVORS)}")
    window.configure(bg="black")
    window.title("Board of the Chords")
    for col in range(25):
        window.columnconfigure(col, minsize=50, weight=2)

    btns = []

    for row, flavor in enumerate(conf.CHORD_FLAVORS):
        window.rowconfigure(row, minsize=5, weight=1)
        for column in range(12):
            root = (column * 7 + conf.CENTERING + flavor.offset) % 12
            b = tk.Button(
                master=window,
                text=flavor.format_string % conf.NOTE_NAMES[root % 12],
                height=5,
                width=5,
            )
            b.bind(
                "<ButtonPress-1>",
                lambda _, notes=flavor.notes, root=root, row=row: start_chord(
                    notes, root, ROW_INSTRUMENTS[row]
                ),
            )

            b.bind(
                "<ButtonRelease-1>", lambda _, row=row: end_chord(ROW_INSTRUMENTS[row])
            )

            b.grid(
                row=row,
                column=column * 2 + row % 2,
                columnspan=2,
                sticky="nsew",
                padx=2,
                pady=2,
            )
            b.config(background="#111111")
            b.config(
                foreground=COLORS[
                    (column - COLOR_ON_C * flavor.offset + conf.CENTERING) % 12
                ]
            )

            btns.append(b)
    window.mainloop()


def qt_simple():
    app = QApplication()

    w = QWidget()

    gl = QGridLayout()

    w.setLayout(gl)
    w.setWindowTitle("Board of the Chords")

    for row, flavor in enumerate(conf.CHORD_FLAVORS):
        for column in range(12):
            root = (column * 7 + conf.CENTERING + flavor.offset) % 12

            b = QPushButton(
                text=flavor.format_string % conf.NOTE_NAMES[root % 12],
            )
            b.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            gl.addWidget(b, row, column * 2 + row % 2, 1, 2)
            b.pressed.connect(
                lambda notes=flavor.notes, root=root, row=row: start_chord(
                    notes, root, ROW_INSTRUMENTS[row]
                ),
            )
            b.released.connect(lambda row=row: end_chord(ROW_INSTRUMENTS[row]))
            color = COLORS[(column - COLOR_ON_C * flavor.offset + conf.CENTERING) % 12]
            b.setStyleSheet(f"background-color: #051105; color: {color}")
            w.setStyleSheet("background-color: black")

    w.show()

    sys.exit(app.exec_())


def foo(*args, **kwargs):
    print(args)
    print(kwargs)


if __name__ == "__main__":
    if conf.GUI_FRAMEWORK == "tk":
        simple()
    else:
        qt_simple()
