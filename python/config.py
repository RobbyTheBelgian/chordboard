import os
from dataclasses import dataclass
from enum import Enum
from dotenv import load_dotenv
import json
from typing import get_type_hints

load_dotenv()

with open(os.environ.get("CONFIG_FILE", "default_config.json"), "r") as configfile:
    config_file = json.load(configfile)


class Arpegiation(Enum):
    NONE = 1
    UP = 2
    DOWN = 3
    UP_OR_DOWN = 4
    RANDOM = 5


# s.print_default_soundfont_presets()


@dataclass
class ChordFlavor:
    notes: list[int]
    format_string: str  # Must require 1 string argument which will contain the root note name, eg "%s-" will show "C-", "Ab-", ...
    offset: int = 0
    instrument: str | None = None
    arpegiation: Arpegiation = Arpegiation.NONE
    arpegiation_length: float | None = None


class AppConfigError(Exception):
    pass


class Config:
    GUI_FRAMEWORK: str = "qt"

    INSTRUMENT: str = "Organ"

    LOWEST: int = 60
    HIGHEST: int = 84

    NOTE_BASIS: int = 72  # This is the note that '0' refers to - 72 is C4.
    CENTERING: int = 6  # Offset where NOTE_BASIS will occur for non-offset chords

    MAX_NOTE_LENGTH: float = 2.0

    ARPEGIATION: Arpegiation = Arpegiation.NONE
    ARPEGIATION_LENGTH: float = 0.5

    NOTE_NAMES: list[str] = [
        "C",
        "C♯",
        "D",
        "E♭",
        "E",
        "F",
        "F♯",
        "G",
        "A♭",
        "A",
        "B♭",
        "B",
    ]

    CHORD_FLAVORS: list[ChordFlavor] = [
        ChordFlavor([0, 4, 7, 10], "%s7"),
        ChordFlavor([0, 4, 7], "%s"),
        ChordFlavor([0, 3, 7], "%sm", offset=9),
        ChordFlavor([0, 3, 7, 11], "%sm(M7)", offset=9),
        ChordFlavor([-12, -5, 0, 7], "%s7"),
        ChordFlavor([0, 3, 6], "%s\N{DEGREE SIGN}", offset=9),
        ChordFlavor([0, 4, 8], "%s+"),
    ]

    def __init__(self, env) -> None:
        for simple_field in [
            "INSTRUMENT",
            "LOWEST",
            "HIGHEST",
            "NOTE_BASIS",
            "CENTERING",
            "MAX_NOTE_LENGTH",
            "ARPEGIATION_LENGTH",
        ]:
            self.__setattr__(simple_field, env.get(simple_field))

        if "GUI_FRAMEWORK" in env:
            self.__setattr__("GUI_FRAMEWORK", env.get("GUI_FRAMEWORK"))


        if "ARPEGIATION" in env:
            self.__setattr__("ARPEGIATION", Arpegiation[env.get("ARPEGIATION")])

        if "CHORD_FLAVORS" in env:
            flavors = []
            for flavor in env.get("CHORD_FLAVORS"):
                if "arpegiation" in flavor:
                    flavor["arpegiation"] = Arpegiation[flavor["arpegiation"]]
                flavors.append(ChordFlavor(**flavor))

            self.__setattr__("CHORD_FLAVORS", flavors)


config = Config(config_file)
